package com.employeeportal.Addressservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.employeeportal.Addressservice.model.Address;
import com.employeeportal.Addressservice.repository.AddressRepository;

@RestController
public class AddressController {

	@Autowired
	AddressRepository addressRepository;
	
	@RequestMapping(path = "/address/add" , method = RequestMethod.POST)
	public String addAddress(@RequestBody Address address) {
		addressRepository.save(address);
		return "Added!!";
	}
	
	
	@GetMapping (path = "/address/get/{id}")
	public Address getAddress(@PathVariable("id") int id) {
		Address ad = addressRepository.findById(id).get();
		return ad;
	}
}
