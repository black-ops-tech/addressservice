package com.employeeportal.Addressservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeeportal.Addressservice.model.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>
{

}
